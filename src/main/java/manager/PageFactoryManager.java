package manager;

import org.openqa.selenium.WebDriver;
import pages.HomePage;
import pages.ResultsPage;
import pages.RuHomePage;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public RuHomePage getRuHomePage() {  return new RuHomePage(driver); }

    public ResultsPage getResultsPage() {
        return new ResultsPage(driver);
    }


}
