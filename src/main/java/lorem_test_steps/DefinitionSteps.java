package lorem_test_steps;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import manager.PageFactoryManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.HomePage;
import pages.ResultsPage;
import pages.RuHomePage;
import java.util.HashSet;
import java.util.List;
import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static java.lang.Integer.parseInt;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.xpath;

public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 60;

    WebDriver driver;
    HomePage homePage;
    RuHomePage ruHomePage;
    ResultsPage resultsPage;
    PageFactoryManager pageFactoryManager;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User switch language to russian")
    public void switchLanguage(){
        homePage.clickLanguageLink();

    }

    @And("User checks that the text of the first element, which is the first paragraph, contains the word «рыба»")
    public void checkContainsWord() throws InterruptedException {
        String fishSubstring = "рыба";
        ruHomePage = pageFactoryManager.getRuHomePage();
        ruHomePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        ruHomePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, ruHomePage.getRuParagraphText());
          Assert.assertTrue(ruHomePage.getText(ruHomePage.getRuParagraphText()).contains(fishSubstring));
    }

    @And("User generates text")
    public void generateText() {
        homePage.clickGenerateButton();
         }

    @And("User checks that the first paragraph starts with \"Lorem ipsum dolor sit amet, consectetur adipiscing elit\"")
    public void isContainsText() {

        resultsPage = pageFactoryManager.getResultsPage();
        resultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        resultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, resultsPage.getParagraphText());
        String fishSubstring = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";
        Assert.assertTrue(resultsPage.getText(resultsPage.getParagraphText()).startsWith(fishSubstring));
    }


    @And("User choose radiobutton Words")
    public void clickRadioButtonWords() {
        homePage.clickRadioButtonWords();
    }

    @And("User choose radiobutton Bytes")
    public void clickRadioButtonBytes() {
        homePage.clickRadioButtonBytes();
    }

    @And("User input {string} into the number field")
    public void typeNumber(final String num) {
        homePage.inputNumberInAmountField(num);
    }

    @And("User check that result has correct {string} of words")
    public void checkNumberOfWords(final String wordsNumber) {
        resultsPage = pageFactoryManager.getResultsPage();
        resultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, resultsPage.getParagraphText());
        int numberOfWords = resultsPage.getText(resultsPage.getParagraphText()).split(" +").length;
        if (parseInt(wordsNumber) <= 0) {
            Assert.assertEquals(5, numberOfWords);
        } else {
            Assert.assertEquals(parseInt(wordsNumber), numberOfWords);
        }
    }

    @And("User check that result has correct {string} of bytes")
    public void checkNumberOfBytes(final String bytesNumber) {
        resultsPage = pageFactoryManager.getResultsPage();
        resultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, resultsPage.getParagraphText());
        int numberOfBytes = resultsPage.getText(resultsPage.getParagraphText()).length();
        if (parseInt(bytesNumber) <= 0) {
            Assert.assertEquals(5, numberOfBytes);
        } else {
            Assert.assertEquals(parseInt(bytesNumber), numberOfBytes);
        }
    }

    @And("User uncheck \"start with Lorem Ipsum\" checkbox")
    public void uncheckCheckBox(){
       homePage.clickLoremStartCheckBox();
    }

    @And("User check that result no longer starts with Lorem ipsum")
    public void checkBeginningOfText(){
        resultsPage = pageFactoryManager.getResultsPage();
        resultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, resultsPage.getParagraphText());
        String fishSubstring = "Lorem ipsum";
        Assert.assertFalse(resultsPage.getText(resultsPage.getParagraphText()).startsWith(fishSubstring));
    }

    @And("User generates text 10 times and checks that amount of paragraphs contain the word \"lorem\" at list in 2 paragraphs")
    public void checkLoremContaining() throws InterruptedException {
        int counterOfParagraphsContainedLorem = 0;
        int sumOfUniqueParagraph = 0;
        HashSet<Integer> amountOfParagraphsContainedLoremPerTransaction = new HashSet<Integer>();

        for(int i=0; i<10; i++){

            homePage.clickGenerateButton();
            resultsPage = pageFactoryManager.getResultsPage();
            resultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, resultsPage.getParagraphText());
            List<WebElement> paragraphsList = driver.findElements(xpath("//*[@id='lipsum']/p"));


            for (int j = 0; j < paragraphsList.size(); j++) {
                String paragraphText = paragraphsList.get(j).getText().toLowerCase();
                if (paragraphText.contains("lorem")) {
                    counterOfParagraphsContainedLorem += 1;
                }
            }

            amountOfParagraphsContainedLoremPerTransaction.add(counterOfParagraphsContainedLorem);
            counterOfParagraphsContainedLorem = 0;
            resultsPage.goToHomePage();
            Thread.sleep(1000);
        }

        for(int s:amountOfParagraphsContainedLoremPerTransaction){
            System.out.println(s);
            sumOfUniqueParagraph = sumOfUniqueParagraph+s;
        }
        System.out.println();
        System.out.println(sumOfUniqueParagraph );
        System.out.println();
        System.out.println(amountOfParagraphsContainedLoremPerTransaction.size() );
        assertTrue((sumOfUniqueParagraph / amountOfParagraphsContainedLoremPerTransaction.size()) > 2);
    }

    @After
    public void tearDown() {
        driver.close();
    }

}
