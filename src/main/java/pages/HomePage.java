package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {



    @FindBy(xpath = "//*[@id='Languages']/a[text()='Pyccкий']")
    private WebElement languageLink;

    @FindBy(xpath = "//*[@id='generate']")
    private WebElement generateButton;

    @FindBy(xpath = "//*[@id='words']")
    private WebElement radioButtonWords;

    @FindBy(xpath = "//*[@id='bytes']")
    private WebElement radioButtonBytes;

    @FindBy(xpath = "//*[@id='start']")
    private WebElement loremStartCheckBox;

    @FindBy(xpath = "//*[@id='amount']")
    private WebElement amountField;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    public RuHomePage clickLanguageLink() {
        languageLink.click();
        return new RuHomePage(driver);
    }

    public void clickGenerateButton() {
        generateButton.click();
    }

    public void clickRadioButtonWords() {
        radioButtonWords.click();
    }

    public void clickRadioButtonBytes() {
        radioButtonBytes.click();
    }

    public void clickLoremStartCheckBox() {
        loremStartCheckBox.click();
    }

    public void inputNumberInAmountField(String num) {
        amountField.clear();
        amountField.sendKeys(num);
    }
    public WebElement getLanguageLink() {
        return languageLink;
    }

}
