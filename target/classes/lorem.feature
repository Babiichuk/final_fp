Feature: Main parameters
  As a user
  I want to test that main features of my site works and follows main parameters


  Scenario Outline: Check that the word "рыба" correctly appears in the first paragraph

    Given User opens '<homePage>' page
    And User switch language to russian
    Then User checks that the text of the first element, which is the first paragraph, contains the word «рыба»


    Examples:
      | homePage                |
      | https://www.lipsum.com/ |


  Scenario Outline: Check that default setting result in text starting with Lorem ipsum

    Given User opens '<homePage>' page
    And User generates text
    Then User checks that the first paragraph starts with "Lorem ipsum dolor sit amet, consectetur adipiscing elit"


    Examples:
      | homePage                |
      | https://www.lipsum.com/ |


  Scenario Outline: Check that Lorem Ipsum is generated with correct amount of words

    Given User opens '<homePage>' page
    And User choose radiobutton Words
    And User input '<wordsNumber>' into the number field
    And User generates text
    Then User check that result has correct '<wordsNumber>' of words

    Examples:
      | homePage                | wordsNumber |
      | https://www.lipsum.com/ | -1          |
      | https://www.lipsum.com/ | 0           |
      | https://www.lipsum.com/ | 5           |
      | https://www.lipsum.com/ | 10          |
      | https://www.lipsum.com/ | 20          |


  Scenario Outline: Check that Lorem Ipsum is generated with correct amount of bytes

    Given User opens '<homePage>' page
    And User choose radiobutton Bytes
    And User input '<bytesNumber>' into the number field
    And User generates text
    Then User check that result has correct '<bytesNumber>' of bytes

    Examples:
      | homePage                | bytesNumber |
      | https://www.lipsum.com/ | -1          |
      | https://www.lipsum.com/ | 0           |
      | https://www.lipsum.com/ | 15          |
      | https://www.lipsum.com/ | 25          |


  Scenario Outline: Check that checkbox works

    Given User opens '<homePage>' page
    And User uncheck "start with Lorem Ipsum" checkbox
    And User generates text
    Then User check that result no longer starts with Lorem ipsum

    Examples:
      | homePage                |
      | https://www.lipsum.com/ |


  Scenario Outline: Check that randomly generated text paragraphs contain the word "lorem" with probability of more than 40%

    Given User opens '<homePage>' page
    And User generates text 10 times and checks that amount of paragraphs contain the word "lorem" at list in 2 paragraphs



    Examples:
      | homePage                |
      | https://www.lipsum.com/ |